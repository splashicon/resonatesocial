<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'resonatesocial');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'qwe012');

/** MySQL hostname */
define('DB_HOST', '192.168.1.8');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+aA)fPu$U&]_S[9T+F)C/=(e-W_b4uo)LinX%[Mvt;f[uN>4~(J;f/ZKQA]Wiot@');
define('SECURE_AUTH_KEY',  'ziZu[ub^n85E_Lo1.iI2;s:o/MaILa!>dRA0.heko5^+Qjz}%;4ha`W<K$X-dZ>V');
define('LOGGED_IN_KEY',    '(+*[0aebXN*NfAb2I>A7-xMsg$=+Y*U)(Jo1I(P,W&i&TyjTOie.6?:JlA&4X}ip');
define('NONCE_KEY',        'N[D5hii8:+oE9QZD25up.hS3@L|Me8|p[ n_O4fo~cc?po7MHR r2^;Y;*0OCuC9');
define('AUTH_SALT',        'Fl&Rk3H4V*@}6BaYL$n}iB $C4;Za,Zt#Q2rAFmAA@`tMwQscTr!eB$J`{*(X*Mr');
define('SECURE_AUTH_SALT', '/r23av}m^&mk]fU%>ZY1qqR(LSMc_i2|dhgj$S-A)xsF;H37HM>Efy`HjO4n&?AD');
define('LOGGED_IN_SALT',   '(`%NJO9H:I&C04Gs/5CeSm1>~G/V[hfh_ox=Z,4kk5:9E8:?aCwb>4w4id9}h@oI');
define('NONCE_SALT',       '0`uMJ,!2VZ-k%`L{F&}rVSzUapi{aS[<mi:$!aAX6Ob8SB.nDGqWSy-OPH)w^3P*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
