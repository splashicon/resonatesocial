<?php
/*
  Plugin Name: Resonate Social
  Description: Provide automated functionality to the order shipped confirmation process
  Version: 0.1
 */

register_activation_hook(__FILE__, 'mps_rs_plugin_install');

function mps_rs_plugin_install() {
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {
		deactivate_plugins( basename( __FILE__ ) );
		$error_message = __('This plugin requires <a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a> plugin to be active!', 'woocommerce');
		die($error_message);
	} else if ( !function_exists("ssh2_connect")) {
		deactivate_plugins( basename( __FILE__ ) );
        die( 'Sorry, but you cannot run this plugin, it requires function ssh2_connect, setup libssh2-php extensions to continue.' );
	} else {
		wp_schedule_event(strtotime('23:55 today'), 'daily', 'mps_scheduled_ftp_connection');
	}
}

add_action('admin_menu', function() {
	add_options_page('Resonate Social', 'Resonate Social', 'manage_options', 'mps-resonate-social', 'mps_resonate_social_page');
});

add_action('admin_init', function() {
	add_settings_section(
			'mps_resonate_social_settings_ftp_section', // ID used to identify this section and with which to register options
			'FTP Credentials', // Title to be displayed on the administration page
			'mps_resonate_social_settings_ftp', // Callback used to render the description of the section
			'mps_resonate_social_page'	// Page on which to add this section of options
	);

	add_settings_field(
			'mps_rs_ftp_server', 'FTP Server', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Server address', 'label_for' => 'mps_rs_ftp_server', 'option' => 'mps_rs_ftp_server', 'placeholder' => 'FTP Server']
	);

	add_settings_field(
			'mps_rs_ftp_server_port', 'FTP Server Port', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Server address', 'label_for' => 'mps_rs_ftp_server_port', 'option' => 'mps_rs_ftp_server_port', 'placeholder' => 'FTP Server Port']
	);

	add_settings_field(
			'mps_rs_ftp_user', 'FTP User Name', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'User Name', 'label_for' => 'mps_rs_ftp_user', 'option' => 'mps_rs_ftp_user', 'placeholder' => 'FTP User Name']
	);

	add_settings_field(
			'mps_rs_ftp_pass', 'FTP Password', 'mps_rs_ftp_server_pass_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Password', 'label_for' => 'mps_rs_ftp_pass', 'option' => 'mps_rs_ftp_pass', 'placeholder' => 'FTP Password']
	);
	add_settings_field(
			'mps_rs_ftp_pub_key', 'Public Key', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Password', 'label_for' => 'mps_rs_ftp_pub_key', 'option' => 'mps_rs_ftp_pub_key', 'placeholder' => 'Full path to Public Key']
	);
	add_settings_field(
			'mps_rs_ftp_priv_key', 'Private Key', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Password', 'label_for' => 'mps_rs_ftp_priv_key', 'option' => 'mps_rs_ftp_priv_key', 'placeholder' => 'Full path to Private Key']
	);
	add_settings_field(
			'mps_rs_ftp_key_phrase', 'Key Passphrase', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Password', 'label_for' => 'mps_rs_ftp_key_phrase', 'option' => 'mps_rs_ftp_key_phrase', 'placeholder' => 'Key Passphrase']
	);

	add_settings_field(
			'mps_rs_ftp_remote_dir', 'Remote Directory', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'Remote Directory', 'label_for' => 'mps_rs_ftp_remote_dir', 'option' => 'mps_rs_ftp_remote_dir', 'placeholder' => 'Remote Directory']
	);

	add_settings_field(
			'mps_rs_ftp_file', 'File Name', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_ftp_section', ['description' => 'File Name', 'label_for' => 'mps_rs_ftp_file', 'option' => 'mps_rs_ftp_file', 'placeholder' => 'File Name']
	);

	add_settings_section(
			'mps_resonate_social_settings_file_options_section', // ID used to identify this section and with which to register options
			'File Options: After Processing', // Title to be displayed on the administration page
			'mps_resonate_social_settings_ftp', // Callback used to render the description of the section
			'mps_resonate_social_page'	// Page on which to add this section of options
	);
	
	add_settings_field(
			'mps_rs_delete_file', 'Delete Remote File', 'mps_rs_ftp_server_checkbox_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_file_options_section', ['label_for' => 'mps_rs_delete_file', 'option' => 'mps_rs_delete_file']
	);
	
	add_settings_field(
			'mps_rs_rename_file', 'Rename Remote File', 'mps_rs_ftp_server_checkbox_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_file_options_section', ['label_for' => 'mps_rs_rename_file', 'option' => 'mps_rs_rename_file']
	);
	add_settings_field(
			'mps_rs_locate_file', 'Save File To', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_settings_file_options_section', ['description' => 'File Location', 'label_for' => 'mps_rs_locate_file', 'option' => 'mps_rs_locate_file', 'placeholder' => 'File Location']
	);
	
	add_settings_section(
			'mps_resonate_social_cron_event_section', // ID used to identify this section and with which to register options
			'Cron Event Options', // Title to be displayed on the administration page
			'mps_resonate_social_settings_ftp', // Callback used to render the description of the section
			'mps_resonate_social_page'	// Page on which to add this section of options
	);
	
	add_settings_field(
			'mps_rs_next_run', 'Next run (UTC)', 'mps_rs_ftp_server_callback', 'mps_resonate_social_page', 'mps_resonate_social_cron_event_section', ['description' => 'Next cron event run', 'label_for' => 'mps_rs_next_run', 'option' => 'mps_rs_next_run', 'placeholder' => 'Next cron event run']
	);
	
	add_settings_field(
			'mps_rs_event_schedule', 'Event schedule', 'mps_schedules_dropdown', 'mps_resonate_social_page', 'mps_resonate_social_cron_event_section', ['description' => 'Next cron event run', 'label_for' => 'mps_rs_event_schedule', 'option' => 'mps_rs_event_schedule', 'placeholder' => 'Next cron event run']
	);

	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_server');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_server_port');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_user');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_pass');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_remote_dir');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_locate_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_rename_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_delete_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_next_run');
	register_setting('mps-resonate-social-settings', 'mps_rs_event_schedule');
	register_setting('mps-resonate-social-settings', 'mps_rs_delete_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_rename_file');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_pub_key');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_priv_key');
	register_setting('mps-resonate-social-settings', 'mps_rs_ftp_key_phrase');
});

function mps_resonate_social_settings_ftp() {
	
}

function mps_resonate_social_page() {
	?>
	<div class="wrap">
		<form action="options.php" method="post">

	<?php
	settings_fields('mps-resonate-social-settings');
	do_settings_sections("mps_resonate_social_page");
	submit_button('Save Settings', 'primary', 'save_mps_rs', true);
	?>
		</form>
	</div>
	<?php
	mps_ftp_connection();
}

function mps_rs_ftp_server_callback($args) {
	$option = get_option($args['option']);
	if ($args['option'] == 'mps_rs_next_run') {
		$option = date( 'Y-m-d H:i:s', wp_next_scheduled( 'mps_scheduled_ftp_connection' ));
	}
	?>
	<input type="text" placeholder="<?= $args['placeholder'] ?>" name="<?= $args['option'] ?>" value="<?php echo esc_attr($option); ?>" size="50" />
	<?php
}

function mps_rs_ftp_server_checkbox_callback($args) {
	$option = get_option($args['option']);
	?>
	<input type="checkbox" name="<?= $args['option'] ?>" <?php echo esc_attr( $option == 'on' ? 'checked="checked"' : ''); ?>/>
	<?php
}

function mps_rs_ftp_server_pass_callback($args) {
	$option = get_option($args['option']);
	?>
	<input type="password" placeholder="<?= $args['placeholder'] ?>" name="<?= $args['option'] ?>" value="<?php echo esc_attr($option); ?>" size="50" />
	<?php
}

add_action('mps_scheduled_ftp_connection', 'mps_ftp_connection');

function mps_ftp_connection() {
	echo '<pre>';
		print_r('Started');
	echo '</pre>';
	if (!function_exists("ssh2_connect"))
		die('Function ssh2_connect not found, you cannot use ssh2 here');
	echo '<pre>';
		print_r('ssh2_connect function found');
	echo '</pre>';

	if (!$connection = ssh2_connect(get_option('mps_rs_ftp_server'), get_option('mps_rs_ftp_server_port')))
		die('Unable to connect');
	echo '<pre>';
		print_r('Connected');
	echo '</pre>';
	
	if (get_option('mps_rs_ftp_pub_key') && get_option('mps_rs_ftp_pub_key')!='') {
		if (!ssh2_auth_pubkey_file($connection, get_option('mps_rs_ftp_user'), get_option('mps_rs_ftp_pub_key'), get_option('mps_rs_ftp_priv_key'), get_option('mps_rs_ftp_key_phrase')))
		die('Unable to authenticate with keys.');
	} else {
		if (!ssh2_auth_password($connection, get_option('mps_rs_ftp_user'), get_option('mps_rs_ftp_pass')))
			die('Unable to authenticate.');
	}
	echo '<pre>';
		print_r('Authenticated');
	echo '</pre>';

	if (!$stream = ssh2_sftp($connection))
		die('Unable to create a stream.');
	echo '<pre>';
		print_r('Stream opened');
	echo '</pre>';

	$remoteDir = get_option('mps_rs_ftp_remote_dir');
	if (!$dir = opendir("ssh2.sftp://{$stream}{$remoteDir}"))
		die('Could not open the directory');
	echo '<pre>';
		print_r('Remote directory opened');
	echo '</pre>';
	$file = get_option('mps_rs_ftp_file');

	if (!$remote = @fopen("ssh2.sftp://{$stream}/{$remoteDir}{$file}", 'r')) {
		die('Unable to open remote file');
	}		
	echo '<pre>';
		print_r('Remote file opened');
	echo '</pre>';
	$read = 0;
	$filesize = filesize("ssh2.sftp://{$stream}/{$remoteDir}{$file}");
	
	while (!feof($remote)) {
		$arr = explode(',', fgets($remote));
		if ($arr[0] == 'O') {
			$order_id = $arr[1];
			$order = wc_get_order($order_id);
			if ($order) {
				update_post_meta($order_id, 'mps_rs_shipping_carrier', $arr[4]);
				update_post_meta($order_id, 'mps_rs_shipping_type', $arr[5]);
				update_post_meta($order_id, 'mps_rs_tracking_number', $arr[6]);
				$order->update_status('completed');
			}
		}
	}
	fclose($remote);
	echo '<pre>';
		print_r('Orders updated');
	echo '</pre>';
	
	if (get_option('mps_rs_locate_file') && get_option('mps_rs_locate_file')!='') {
		
		if (!$remote = @fopen("ssh2.sftp://{$stream}/{$remoteDir}{$file}", 'r')) {
			die('Unable to open remote file');
		}		
		$localDir = get_option('mps_rs_locate_file');

		if (!$local = @fopen($localDir . $file, 'w')) {
			die('Unable to create local file');
		}
		$read = 0;
		$filesize = filesize("ssh2.sftp://{$stream}/{$remoteDir}{$file}");
		while ($read < $filesize && ($buffer = fread($remote, $filesize - $read))) {
			$read += strlen($buffer);
			if (fwrite($local, $buffer) === FALSE) {
				echo "Unable to write to local file: $file\n";
				break;
			}
		}

		fclose($local);
		fclose($remote);
	}
	
	if (get_option('mps_rs_rename_file') && get_option('mps_rs_rename_file')=='on') {
		$path_parts = pathinfo($remoteDir.$file);
		ssh2_sftp_rename($stream, $remoteDir.$file, $remoteDir.$path_parts['filename'].'_'.time().'.'.$path_parts['extension']);
	}
	
	if (get_option('mps_rs_delete_file') && get_option('mps_rs_delete_file')=='on') {
		ssh2_sftp_unlink($stream, $remoteDir.$file);
	}
	
	echo '<pre>';
		print_r('finished');
	echo '</pre>';
	
}

function mps_get_schedules() {
	$schedules = wp_get_schedules();
	uasort($schedules, create_function('$a, $b', 'return $a["interval"] - $b["interval"];'));
	return $schedules;
}

function mps_schedules_dropdown($args) {
	$current = get_option($args['option']);
	$schedules = mps_get_schedules();
	?>
			<select class="postform" name="<?= $args['option'] ?>" id="<?= $args['option'] ?>">
			<option <?php selected($current, '_oneoff'); ?> value="_oneoff"><?php esc_html_e('Non-repeating', 'wp-crontrol'); ?></option>
	<?php foreach ($schedules as $sched_name => $sched_data) { ?>
					<option <?php selected($current, $sched_name); ?> value="<?php echo esc_attr($sched_name); ?>">
		<?php
		printf(
				'%s (%s)', esc_html($sched_data['display']), esc_html(mps_interval($sched_data['interval']))
		);
		?>
					</option>
	<?php } ?>
			</select>
	<?php
}

function mps_interval( $since ) {
		// array of time period chunks
		$chunks = array(
			array( 60 * 60 * 24 * 365, _n_noop( '%s year', '%s years', 'wp-crontrol' ) ),
			array( 60 * 60 * 24 * 30, _n_noop( '%s month', '%s months', 'wp-crontrol' ) ),
			array( 60 * 60 * 24 * 7, _n_noop( '%s week', '%s weeks', 'wp-crontrol' ) ),
			array( 60 * 60 * 24, _n_noop( '%s day', '%s days', 'wp-crontrol' ) ),
			array( 60 * 60, _n_noop( '%s hour', '%s hours', 'wp-crontrol' ) ),
			array( 60, _n_noop( '%s minute', '%s minutes', 'wp-crontrol' ) ),
			array( 1, _n_noop( '%s second', '%s seconds', 'wp-crontrol' ) ),
		);

		if ( $since <= 0 ) {
			return __( 'now', 'wp-crontrol' );
		}

		for ( $i = 0, $j = count( $chunks ); $i < $j; $i++ ) {
			$seconds = $chunks[ $i ][0];
			$name = $chunks[ $i ][1];

			// finding the biggest chunk (if the chunk fits, break)
			if ( ( $count = floor( $since / $seconds ) ) != 0 ) {
				break;
			}
		}

		// set output var
		$output = sprintf( translate_nooped_plural( $name, $count, 'wp-crontrol' ), $count );

		// step two: the second chunk
		if ( $i + 1 < $j ) {
			$seconds2 = $chunks[ $i + 1 ][0];
			$name2 = $chunks[ $i + 1 ][1];

			if ( ( $count2 = floor( ( $since - ( $seconds * $count ) ) / $seconds2 ) ) != 0 ) {
				// add to output var
				$output .= ' ' . sprintf( translate_nooped_plural( $name2, $count2, 'wp-crontrol' ), $count2 );
			}
		}

		return $output;
	}

add_filter( 'cron_schedules', 'mps_add_cron_30min_interval' );

function mps_add_cron_30min_interval($schedules) {
	$schedules['30_min'] = array(
		'interval' => 1800,
		'display' => esc_html__('Every 30 Minutes'),
	);

	return $schedules;
}

add_action( 'init', 'mps_rs_action_handle_posts' );

function mps_rs_action_handle_posts() {
	if ( isset( $_POST['save_mps_rs'] ) ) {
		wp_clear_scheduled_hook('mps_scheduled_ftp_connection');
		wp_schedule_event(strtotime($_POST['mps_rs_next_run']), $_POST['mps_rs_event_schedule'], 'mps_scheduled_ftp_connection');
	}
}

add_action( 'woocommerce_email_before_order_table', 'mps_rs_add_tracker_data', 10, 1);

function mps_rs_add_tracker_data($order) {
	$post_id = $order->ID;
	if ( get_post_meta($post_id, 'mps_rs_shipping_carrier', true) ) {
	?>
		<div>Shipping Carrier: <?= get_post_meta($post_id, 'mps_rs_shipping_carrier', true) ?></div>
	<?php 
	}
	if ( get_post_meta($post_id, 'mps_rs_shipping_type', true) ) {
	?>
		<div>Shipping Type: <?= get_post_meta($post_id, 'mps_rs_shipping_type', true) ?></div>
	<?php 
	}
	if ( get_post_meta($post_id, 'mps_rs_tracking_number', true) ) {
	?>
		<div>Tracking Number: <a href="https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=<?= get_post_meta($post_id, 'mps_rs_tracking_number', true) ?>"><?= get_post_meta($post_id, 'mps_rs_tracking_number', true) ?></a></div></br>
	<?php 
	}
}