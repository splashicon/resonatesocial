<?php
/**
 * WooCommerce Customer/Order CSV Export
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Customer/Order CSV Export to newer
 * versions in the future. If you wish to customize WooCommerce Customer/Order CSV Export for your
 * needs please refer to http://docs.woocommerce.com/document/ordercustomer-csv-exporter/
 *
 * @package     WC-Customer-Order-CSV-Export/Export-Methods/SFTP
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2017, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

defined( 'ABSPATH' ) or exit;

/**
 * Export SFTP Class
 *
 * Simple wrapper for ssh2_* functions to upload an exported file to a remote
 * server via FTP over SSH
 *
 * @since 3.0.0
 */
class WC_Customer_Order_CSV_Export_Method_SFTP_EMAIL extends WC_Customer_Order_CSV_Export_Method_File_Transfer {


	/** @var resource sftp connection resource */
	private $sftp_link;
	/** @var string email recipients */
	private $email_recipients;

	/** @var string email subject */
	private $email_subject;

	/** @var string email message */
	private $email_message;

	/** @var string email id */
	private $email_id;


	/**
	 * Connect to SSH server, authenticate via password, and set up SFTP link
	 *
	 * @since 3.0.0
	 * @see WC_Customer_Order_CSV_Export_Method_File_Transfer::__construct()
	 * @throws SV_WC_Plugin_Exception - ssh2 extension not installed, failed SSH / SFTP connection, failed authentication
	 * @param array $args
	 */
	 public function __construct( $args ) {

	 	parent::__construct( $args );
		
		$this->email_recipients = $args['email_recipients'];
		$this->email_subject    = $args['email_subject'];
		$this->email_message    = $args['email_message'];
		$this->email_id         = $args['email_id'];

		// Handle errors from ssh2_* functions that throw warnings for things like
		// failed connections, etc
		set_error_handler( array( $this, 'handle_errors' ) );

		// check if ssh2 extension is installed
		if ( ! function_exists( 'ssh2_connect' ) ) {

			throw new SV_WC_Plugin_Exception( __( 'SSH2 Extension is not installed, cannot connect via SFTP.', 'woocommerce-customer-order-csv-export' ) );
		}

		// setup connection
//		$this->ssh_link = ssh2_connect( $this->server, $this->port );
		$this->ssh_link = new Net_SFTP($this->server);

		// check for successful connection
		if ( ! $this->ssh_link ) {

			/* translators: Placeholders: %1$s - server address, %2$s - server port. */
			throw new SV_WC_Plugin_Exception( sprintf( __( 'Could not connect via SSH to %1$s on port %2$s, check server address and port.', 'woocommerce-customer-order-csv-export' ), $this->server, $this->port ) );
		}

		try {
			$keyFile = file_get_contents($this->private_key_path, FILE_USE_INCLUDE_PATH);
		} catch(Exception $e) {
			throw new SV_WC_Plugin_Exception('Exception '. $e);
		}
		
		$key = new Crypt_RSA();
		$key->loadKey($keyFile);
		
		if (!$this->ssh_link->login($this->username, $this->password, $key)) {
			throw new SV_WC_Plugin_Exception('Login Failed');
		} 
		
		// setup SFTP link
		$this->sftp_link = $this->ssh_link;
		// check for successful SFTP link
		if ( ! $this->sftp_link ) {
			throw new SV_WC_Plugin_Exception( __( 'Could not setup SFTP link', 'woocommerce-customer-order-csv-export' ) );
		}
	}


	/**
	 * Open remote file and write exported data into it
	 *
	 * @since 3.0.0
	 * @param string $file_path path to file to upload
	 * @throws SV_WC_Plugin_Exception Open remote file failure or write data failure
	 * @return bool whether the upload was successful or not
	 */
	public function perform_action( $file_path ) {

		if ( empty( $file_path ) ) {
			throw new SV_WC_Plugin_Exception( __( 'Missing file path', 'woocommerce-customer-order-csv-export' ) );
		}
		
		// init email args
		$mailer  = WC()->mailer();
		$to      = ( $email = $this->email_recipients ) ? $email : get_option( 'admin_email' );
		$subject = $this->email_subject;
		$message = $this->email_message;

		// Allow actors to change the email headers.
		$headers      = apply_filters( 'woocommerce_email_headers', "Content-Type: text/plain\r\n", $this->email_id );
		$attachments  = array( $file_path );

		// hook into `wp_mail_failed` and throw errors as exceptions
		add_action( 'wp_mail_failed', array( $this, 'handle_wp_mail_error' ) );

		// send email
		$result = $mailer->send( $to, $subject, $message, $headers, $attachments );

		// unhook from wp_mail_failed
		remove_action( 'wp_mail_failed', array( $this, 'handle_wp_mail_error' ) );

		$filename    = basename( $file_path );

		$data = file_get_contents( $file_path );

		if ( false === $data ) {
			/* translators: Placeholders: %s - file name */
			throw new SV_WC_Plugin_Exception( sprintf( __( 'Could not open file %s for reading.', 'woocommerce-customer-order-csv-export' ), $filename ) );
		}

		try {
			$this->sftp_link->put($this->path.$filename, $data);
		} catch(Exception $e) {
			throw new SV_WC_Plugin_Exception('Exception '. $e);
		}

		return true;
	}


	/**
	 * Handle PHP errors during the upload process -- some ssh2_* functions throw E_WARNINGS in addition to returning false
	 * when encountering incorrect passwords, etc. Using a custom error handler serves to return helpful messages instead
	 * of "cannot connect" or similar.
	 *
	 * @since 4.0.5
	 * @param int $error_no unused
	 * @param string $error_string PHP error string
	 * @param string $error_file PHP file where error occurred
	 * @param int $error_line line number of error
	 * @return boolean false
	 * @throws SV_WC_Plugin_Exception
	 */
	public function handle_errors( $error_no, $error_string, $error_file, $error_line ) {

		// only handle errors for our own files
		if ( false === strpos( $error_file, __FILE__ ) ) {

			return false;
		}

		/* translators: Placeholders: %s - error message */
		throw new SV_WC_Plugin_Exception( sprintf( __( 'SFTP error: %s', 'woocommerce-customer-order-csv-export' ), $error_string ) );
	}


	/**
	 * Restore error handler and close SSH connction
	 *
	 * @since 4.0.5
	 */
	public function __destruct() {

		if ( isset( $this->ssh_link ) ) {

			unset( $this->ssh_link );
		}

		// give error handling back to PHP
		restore_error_handler();
	}
	
	public function handle_wp_mail_error( WP_Error $error ) {

		// unhook from wp_mail_failed
		remove_action( 'wp_mail_failed', array( $this, 'handle_wp_mail_error' ) );

		throw new SV_WC_Plugin_Exception( $error->get_error_message() );
	}


}
